import yfinance as yf
from ideas_provider import suggest_company


def give_advice() -> dict:
    while True:
        company = suggest_company()
        ticker = yf.Ticker(company)
        info: dict = ticker.info
        action = info.get('recommendationKey', 'none')
        if action == 'none':
            print(f'cant decide on {company}, let me think some more...')
        else:
            return {'code': company,
                    'name': info['longName'],
                    'recommendation': info['recommendationKey'],
                    'price': info['currentPrice'],
                    }


if __name__ == '__main__':
    advice = give_advice()
    print(f'{advice["recommendation"]} some {advice["name"]} stocks for {advice["price"]}')
